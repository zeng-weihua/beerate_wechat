define(function(){
    /*菜单按钮 公共*/
    $(".header_menu").on("touchend",menu_switch);
    $(".header_close").on("touchend",munu_close);
    function menu_switch(){
        $(".header_nav,.header_close,.header_wrap,.header_menu,.center").toggleClass('curr');
    }
    function munu_close(){
        setTimeout(function(){
            $(".header_nav,.header_close,.header_wrap,.header_menu,.center").toggleClass('curr');
        },100)
    }
    //  项目集下拉选择
    $(function(){
        if($(".project_nav").length > 0){
            var _li = $(".project_nav").children("li");
            var leng = _li.length;
            if(leng > 3){
                _li.css({"width":"33.3%"})
            }
        }
    });
    $(".project_select").on("touchend",menu_select);
    function menu_select(){
        var project_nav =$(".project_nav");      
        if(project_nav.is(":hidden")){
            $(".project_mask").show();
            project_nav.slideDown();
            $(".project_select").addClass("curr");
        }else{
            project_nav.slideUp();
            $(".project_mask").hide();
            $(".project_select").removeClass("curr");
        } 
    }
    //  筛选
    $(function(){
        $(".project_main_ul2").each(function(){
            var filter_li = $(this).children("li");
            var _index = filter_li.length;
            if(_index > 3){
                filter_li.css({"width":"33.3%"})
            }
            filter_li.click(function(e){
                $(this).addClass("curr").siblings("li").removeClass("curr");
                e.stopPropagation();
            })
        })
        //  项目集筛选选择
        $(".project_filtername").on("touchend",filter_nav);
        function filter_nav(){
            $(this).parent("li").siblings().removeClass("curr");
            $(this).parent("li").siblings().find(".project_main_ul2").slideUp();
            var filter = $(this).siblings(".project_main_ul2"); 
            if(filter.is(":hidden")){
                filter.slideDown();
                $(this).parent("li").addClass("curr");
                $(".project_mask").show();
            }
            else{
                filter.slideUp();
                $(this).parent("li").removeClass("curr");
                $(".project_mask").hide();
            } 
        }
        //  关闭选择
        $(".project_mask").on("touchend",filter_close);
        function filter_close(){
            $(".project_nav").slideUp();
            $(".project_select").removeClass("curr");
            $(".project_main_ul2").slideUp();
            $(".project_main_nav").removeClass("curr");
            setTimeout(function(){
                $(".project_mask").hide();
            },200)
        }
    })
    /*底部居底*/
    $(document).ready(function(){
        var win_hei=$(window).outerHeight(true);
        var bghei=$(".center").outerHeight(true);
        var bot_hei=$(".footer").outerHeight(true);
        console.log(win_hei,bghei,bot_hei)
        if (bghei<(win_hei - bot_hei)) {
            $('.center').wrap('<div class="y-wrap"></div>');
            $('.y-wrap').css('min-height',win_hei - bot_hei+5);
            // 处理项目集没产品
            $(".project_c_main").css('min-height',win_hei - bot_hei+5);

        }
    })
    //  业务模块
    $(function(){
        // 锚点
        setTimeout(function(){
            var url = document.location.href;
            url_xin = url.substring(url.lastIndexOf("=")+1);
            if(url_xin === 1){
                $('html, body').animate({
                    scrollTop: $(".product_c_box2").offset().top
                }, 500);
            }else if(url_xin === 2){
                $('html, body').animate({
                    scrollTop: $(".product_c_box3").offset().top
                }, 500);
            }else if(url_xin === 3){
                $('html, body').animate({
                    scrollTop: $(".product_c_box4").offset().top
                }, 500);
            }else{
                return url;
                console.log(url);
            }
        },300)
    })

})


