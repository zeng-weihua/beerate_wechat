require.config({
	paths: {
		"jquery": "lib/jquery", //jquery包
		"skitter": "jquery.skitter.min",
		"easing": "jquery.easing.1.3",
		"fancyBox": "fancyBox/jquery.fancybox",
		"thumbs": "fancyBox/helpers/jquery.fancybox-thumbs",
		"Jcrop": "jquery.Jcrop.min",
		"script": "script",
		"flexslider_min": "flexslider_min", // 轮播库
		'vue': 'vue', // vue
		'vueResource': 'vueResource', //vue的ajax插件
		'vueDataInterface': 'vueDataInterface',
		'ELEMENT': 'vueElement' // vue element ui 组件
	},
	shim: {
		vue:{
			exports: 'vue'
		},
		vueResource:{
			exports: 'vueResource'
		},
		vueDataInterface:{
			exports: 'vueDataInterface'
		},
		easing: {
			deps: ['jquery']
		},
		skitter: {
			deps: ['easing'],
			exports: 'skitter'
		},
		fancyBox: {
			deps: ["jquery"],
			exports: "window,document,jQuery"
		},
		thumbs: {
			deps: ["jquery","fancyBox"],
			exports: "window,document,jQuery"
		},
		Jcrop: {
			deps: ["jquery"],
			exports: "jQuery"
		},
		script:{
			deps:["jquery"],
			exports: "script"
		},
		flexslider_min:{
			deps:["jquery"],
			exports: "flexslider_min"
		}
	}
});
define(['jquery','script','flexslider_min'],function($){
	//选项卡
	var tabcard = function(menu,cont){
			menu.eq(0).addClass('curr');
			cont.hide().eq(0).show();
			menu.click(function(){
				var index = $(this).index();
				$(this).addClass('curr').siblings().removeClass('curr');
				cont.hide().eq(index).slideDown(500);
			});
		}
	tabcard($('.detail-menu').children(),$('.tab-cont').children());

	return {
		// 为当前页面的导航添加选中后的样式
		navSelected: function(index){
			$(".nav_ul>li").eq(index).addClass("curr");
		},
		tabcard: tabcard
	};
});
