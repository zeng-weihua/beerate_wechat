define(['jquery','vue','vueResource','ELEMENT','cookies'],function ($,Vue,VueResource,ELEMENT) {
    // 数据请求 服务网址
    const baseUrl = 'http://192.168.0.129:8081';
    const indexUrl = 'index.html';    // 没参数时 跳转首页
    const SuccessUrl = '404.html';    // 没参数时 跳转404

    // 数据中心 股票代码错误返回
    var data_error = function (){
        $(".data_error_pop_up").show();
        $("#container").hide();
        var dataTime = $(".data_error_time").text();
        var timeid;
        timeid = setInterval(function(){
            if(dataTime > 0){
                dataTime--;
                $(".data_error_time").text(dataTime);
            }else{
                clearInterval(timeid);
                document.location.href = indexUrl;
            }
        },1000);
    };

    // 获取参数
    const getRequest = function() {
        var url = location.search; //获取url中"?"符后的字串
        var theRequest = new Object();
        if (url.indexOf("?") != -1) {
            var str = url.substr(1);
            strs = str.split("&");
            for (var i = 0; i < strs.length; i++) {
                theRequest[strs[i].split("=")[0]] = unescape(strs[i].split("=")[1]);
            }
        }else{
            //document.location.href = SuccessUrl;
            data_error();
        }
        return theRequest;
    }
     
    var gpmc = "";//股票名称        
    var gpdm = "";//股票号码     
    var dbmc = "";//对比名称
    // 股票头部信息
    var data_head_wrap = function (){
        setInterval(data_head, 45000);
        var params = getRequest();
        var code = params.code;
        function data_head() {
            var num = 2;
            if(code >= 600000){num = 1;}
            var url = "http://nufm.dfcfw.com/EM_Finance2014NumericApplication/JS.aspx?type=CT&cmd=" + code + num + "&sty=FOF2&st=z&sr=&p=&ps=&cb=?&token=a095a7a834d7707fc2a248070561abed";
            console.log(url)
            $.ajax({
                type: "get",
                url: url,
                dataType: 'jsonp',
                jsonp: 'cb',
                success: function (json) {
                    var jsonToStr = JSON.stringify(json)
                    setCookie("data_head_"+code,jsonToStr);
                    dataCookie(jsonToStr);
                }
            });
        }
        function dataCookie(strToJson){
            var json = JSON.parse(strToJson)
            if (!json || json.length <= 0){return};
                var arry = json[0].split(',');
                    gpmc = arry[2];                         // 股票名称
                    gpdm = arry[1];                         // 股票号码
               var  zxjg = arry[5],                         // 最新价格
                    zdje = arry[10],                        // 涨跌金额
                    zdfd = arry[11],                        // 涨跌幅度
                    jk = arry[4],                           // 今开
                    zg = arry[6],                           // 最高
                    hs = arry[19],                          // 换手
                    zs = arry[3],                           // 昨收
                    zd = arry[7],                           // 最低
                    zt = (zs*1 + zs * 0.1).toFixed(2),      // 涨停
                    dt = (zs*1 - zs * 0.1).toFixed(2),      // 跌停
                    cjl = (arry[9] / 10000).toFixed(2);     // 成交量
                if(zxjg == '-'){
                    zxjg = "停牌";
                    $(".data_news_gpmc").text(gpmc);
                    $(".data_news_gpdm").text(gpdm);
                    $(".databulk_head_price").text(zxjg).addClass('red');
                    $(".databulk_head_zs").text(zs);
                }else{
                    $(".data_news_gpmc").text(gpmc);
                    $(".data_news_gpdm").text(gpdm);
                    $(".databulk_head_price").text(zxjg);
                    $(".datanews_head_par_01").text(zdje);
                    $(".datanews_head_par_02").text(zdfd + '%');
                    $(".databulk_head_zt").text(zt);
                    $(".databulk_head_zg").text(zg);
                    $(".databulk_head_zs").text(zs);
                    $(".databulk_head_dt").text(dt);
                    $(".databulk_head_zd").text(zd);
                    $(".databulk_head_jk").text(jk);
                    $(".databulk_head_hs").text(hs + '%');
                    $(".databulk_head_cjl").text(cjl + '万');
                    if(zdje < 0){
                        $(".datanews_head_par,.databulk_head_price").addClass('green');
                    }else{
                        $(".databulk_head_price").addClass('red');
                    }
                    setTimeout(function(){
                        var ltg_val = $(".databulk_head_ltg").text();
                        var zsz_val = $(".databulk_head_zsz").text();
                        var ltgl = ltg_val.substring(0,ltg_val.length-2);
                        var zsz = zsz_val.substring(0,zsz_val.length-1);
                        var gj_val = $(".databulk_head_price").text();
                        var ltsz = (ltgl * gj_val).toFixed(2);
                        if(ltsz >= zsz){
                            $(".databulk_head_ltsz").text(zsz + '亿');
                        }else{
                            $(".databulk_head_ltsz").text(ltsz + '亿');
                        }
                    },400)
                }
        }
        function init(){
            var cvalue = getCookie("data_head_"+code);
            if(cvalue != null){
                var array = new Array(cvalue);
                dataCookie(array);
            }else{
                data_head();
            }
        }
        init();
        setInterval(init, 2000);
    }

    Vue.use(VueResource); // vue ajax路由
    Vue.use(ELEMENT); // vue element ui 组件
    
    // 公司大事 数据请求
    var data_bulk = function (){
        new Vue({
            el: '.datanews_body',
            data: {
                lists:[],
                gqzy:[],
                xsjj:[],
                sdgdcgbd:[], 
                ggcgbd:[],
                dzjy:[],
                url: baseUrl + '/companyBigNews',  //存数据接口 
                urlHead: baseUrl + '/profile',  //存数据接口   
                head: [],    
                message: '暂无数据',
                code: '',
                gqzyLength: 0,  // 股权质押数量
                dwdbLength: 0,  // 对外担保数量
                sszcLength: 0,  // 诉讼仲裁数量
                wgclLength: 0,  // 违规处理数量
                xsjjLength: 0,  // 限售解禁数量
                sdgdcgbdLength: 0,  // 十大股东持股变动数量
                ggcgbdLength: 0,  // 高管持股变动数量
                dzjyLength: 0,  // 大宗交易数量
                cszsShu: 5,  // 初始加载数量
                jzgdShu: 5  // 加载更多数量
            },
            created: function(){
                var params = getRequest();
                this.code = params.code;
                this.getHead()
                this.getData()      //定义方法
            },
            methods: {
                getData: function(){
                    var that = this;   
                    that.$http({      //调用接口
                        method:'GET',
                        url:(this.url + '?code=' + this.code) //this指data
                    }).then(function(response){ //接口返回数据
                        if(response.data.code == 200){
                            this.lists = response.data.data; 
                            this.gqzy = this.lists.gqzy;
                            this.xsjj = this.lists.xsjj;
                            this.sdgdcgbd = this.lists.sdgdcgbd;
                            this.ggcgbd = this.lists.ggcgbd;
                            this.dzjy = this.lists.dzjy;
                            this.gqzyLength = this.lists.gqzy.length;
                            this.dwdbLength = this.lists.dwdb.length;
                            this.sszcLength = this.lists.sszc.length;
                            this.wgclLength = this.lists.wgcl.length;
                            this.xsjjLength = this.lists.xsjj.length;
                            this.sdgdcgbdLength = this.lists.sdgdcgbd.length;
                            this.ggcgbdLength = this.lists.ggcgbd.length;
                            this.dzjyLength = this.lists.dzjy.length;
                            console.log(this.lists);
                        }else{
                            this.lists = response.data.msg;  
                            console.log(response.data.msg);
                            data_error()
                        } 
                    },function(error){
                        data_error()
                        console.log('无效的参数');
                    })
                },
                gqzyMore: function(){   // 股权质押更多
                    var that = this;
                    $(".data_more_less").hide(),$(".live-upload").show();
                    if(that.cszsShu < that.gqzyLength){
                        setTimeout(function(){
                            $(".live-upload").hide(),$(".data_more_less").show();
                            that.cszsShu = that.cszsShu + that.jzgdShu;
                        },500)
                    }else{
                        setTimeout(function(){
                            $(".live-upload").hide(),$(".data_more_less").show();
                            $(".data_more_wrap_gd").removeClass("curr").text("暂无更多数据")
                        },200)
                    }
                },
                dwdbMore: function(){   // 对外担保更多
                    var that = this;
                    $(".data_more_less").hide(),$(".live-upload").show();
                    if(that.cszsShu < that.dwdbLength){
                        setTimeout(function(){
                            $(".live-upload").hide(),$(".data_more_less").show();
                            that.cszsShu = that.cszsShu + that.jzgdShu;
                        },500)
                    }else{
                        setTimeout(function(){
                            $(".live-upload").hide(),$(".data_more_less").show();
                            $(".data_more_wrap_gd").removeClass("curr").text("暂无更多数据")
                        },200)
                    }
                },
                sszcMore: function(){   // 诉讼仲裁更多
                    var that = this;
                    $(".data_more_less").hide(),$(".live-upload").show();
                    if(that.cszsShu < that.sszcLength){
                        setTimeout(function(){
                            $(".live-upload").hide(),$(".data_more_less").show();
                            that.cszsShu = that.cszsShu + that.jzgdShu;
                        },500)
                    }else{
                        setTimeout(function(){
                            $(".live-upload").hide(),$(".data_more_less").show();
                            $(".data_more_wrap_gd").removeClass("curr").text("暂无更多数据")
                        },200)
                    }
                },
                wgclMore: function(){   // 违规处理更多
                    var that = this;
                    $(".data_more_less").hide(),$(".live-upload").show();
                    if(that.cszsShu < that.wgclLength){
                        setTimeout(function(){
                            $(".live-upload").hide(),$(".data_more_less").show();
                            that.cszsShu = that.cszsShu + that.jzgdShu;
                        },500)
                    }else{
                        setTimeout(function(){
                            $(".live-upload").hide(),$(".data_more_less").show();
                            $(".data_more_wrap_gd").removeClass("curr").text("暂无更多数据")
                        },200)
                    }
                },
                xsjjMore: function(){   // 限售解禁更多
                    var that = this;
                    $(".data_more_less").hide(),$(".live-upload").show();
                    if(that.cszsShu < that.xsjjLength){
                        setTimeout(function(){
                            $(".live-upload").hide(),$(".data_more_less").show();
                            that.cszsShu = that.cszsShu + that.jzgdShu;
                        },500)
                    }else{
                        setTimeout(function(){
                            $(".live-upload").hide(),$(".data_more_less").show();
                            $(".data_more_wrap_gd").removeClass("curr").text("暂无更多数据")
                        },200)
                    }
                },
                sdgdcgbdMore: function(){   // 十大股东持股变动更多
                    var that = this;
                    $(".data_more_less").hide(),$(".live-upload").show();
                    if(that.cszsShu < that.sdgdcgbdLength){
                        setTimeout(function(){
                            $(".live-upload").hide(),$(".data_more_less").show();
                            that.cszsShu = that.cszsShu + that.jzgdShu;
                        },500)
                    }else{
                        setTimeout(function(){
                            $(".live-upload").hide(),$(".data_more_less").show();
                            $(".data_more_wrap_gd").removeClass("curr").text("暂无更多数据")
                        },200)
                    }
                },
                ggcgbdMore: function(){   // 高管持股变动更多
                    var that = this;
                    $(".data_more_less").hide(),$(".live-upload").show();
                    if(that.cszsShu < that.ggcgbdLength){
                        setTimeout(function(){
                            $(".live-upload").hide(),$(".data_more_less").show();
                            that.cszsShu = that.cszsShu + that.jzgdShu;
                        },500)
                    }else{
                        setTimeout(function(){
                            $(".live-upload").hide(),$(".data_more_less").show();
                            $(".data_more_wrap_gd").removeClass("curr").text("暂无更多数据")
                        },200)
                    }
                },
                dzjyMore: function(){   // 大宗交易更多
                    var that = this;
                    $(".data_more_less").hide(),$(".live-upload").show();
                    if(that.cszsShu < that.dzjyLength){
                        setTimeout(function(){
                            that.cszsShu = that.cszsShu + that.jzgdShu;
                            $(".live-upload").hide(),$(".data_more_less").show();
                        },500)
                    }else{
                        setTimeout(function(){
                            $(".live-upload").hide(),$(".data_more_less").show();
                            $(".data_more_wrap_gd").removeClass("curr").text("暂无更多数据")
                        },200)
                    }
                },
                lickLess: function(){   // 收起数据
                    var that = this;
                    that.cszsShu = 5;
                    $(".data_more_wrap_gd").addClass("curr").text("显示更多")
                },
                getHead: function(){
                    var that = this;   
                    that.$http({      //调用接口
                        method:'GET',
                        url:(this.urlHead + '?code=' + this.code) //this指data
                    }).then(function(response){ //接口返回数据
                        if(response.data.code == 200){
                            this.head = response.data.data;
                        }else{
                            this.lists = response.data.msg;  
                        } 
                    })
                }
            }
        })
    };

    // 分红融资 数据请求
    var data_divfin = function (){
        var demo = new Vue({
            el:'#container',
            data: {
                lists:[],  
                url: baseUrl + '/bonusFinancing',  //存数据接口
                urlHead: baseUrl + '/profile',  //存数据接口   
                head: [],
                message: '暂无数据',
                code: '' 
            },
            created: function(){
                var params = getRequest();
                this.code = params.code;
                this.getHead()
                this.getData()      //定义方法
            },
            methods: {
                getData: function(){
                    var that = this;   
                    that.$http({      //调用接口
                        method:'GET',
                        url:(this.url + '?code=' + this.code) //this指data
                    }).then(function(response){ //接口返回数据
                        if(response.data.code == 200){
                            this.lists = response.data.data;
                            console.log(this.lists);
                        }else{
                            this.lists = response.data.msg;  
                            console.log(response.data.msg);
                            data_error()
                        } 
                    },function(error){
                        console.log('无效的参数');
                        data_error()
                    })
                },
                getHead: function(){
                    var that = this;   
                    that.$http({      //调用接口
                        method:'GET',
                        url:(this.urlHead + '?code=' + this.code) //this指data
                    }).then(function(response){ //接口返回数据
                        if(response.data.code == 200){
                            this.head = response.data.data;
                        }else{
                            this.lists = response.data.msg;  
                        } 
                    })
                }
            }
        })
    };

    // 研究报告 数据请求
    var data_resrep = function (){
        new Vue({
            el:'#container',
            data: {
                lists:[],  
                url: baseUrl + '/researchReport',  // 存数据接口   
                urlHead: baseUrl + '/profile',  // 存数据接口   
                head: [],  
                message: '暂无数据',
                urlGsgg: baseUrl + '/getAnnouncements', // 公司公告 存数据接口 
                gsgg:[],                                // 公司公告
                gsggAll:[],                             // 公司公告 全部数据
                beginDate: '2000-01-01',                // 公司公告 开始时间 最早
                endDate: '',                // 公司公告 结束时间
                code: ''  ,
                page: 1,  // 公司公告 显示的是哪一页
                totalPage: Number,  // 公司公告 总共页数
                pageSize: 30, // 公司公告 每一页显示的数据条数
                total: Number, // 公司公告 记录总数
                maxPage:9,  // 最大页数
            },
            created: function(){
                var params = getRequest();
                this.code = params.code;
                this.getHead()
                this.getDate()
            },
            methods: {
                getDate: function(){
                    var that = this;
                    function getNow(s) {
                        return s < 10 ? '0' + s: s;
                    }
                    var myDate = new Date();
                    var year=myDate.getFullYear();// 获取当前年
                    var month=myDate.getMonth()+1;// 获取当前月
                    var date=myDate.getDate();// 获取当前日
                    that.endDate = year+'-'+getNow(month)+"-"+getNow(date)
                    that.getGsgg()
                },
                getGsgg: function(){        // 公司公告
                    var that = this;   
                    that.$http({      // 调用接口
                        method:'GET',
                        url:(this.urlGsgg + '?code=' + this.code + '&currPage=' + this.page + '&pageSize=' + this.pageSize + '&beginDate=' + this.beginDate + '&endDate=' + this.endDate) // this指data
                    }).then(function(response){ // 接口返回数据
                        if(response.data.code == 200){
                            that.gsgg = response.data.data;
                            that.gsggAll= that.gsggAll.concat(response.data.data.announcements); // 储存更多数据
                            this.total = this.gsgg.totalAnnouncement;
                            this.totalPage = Math.ceil(this.total / this.pageSize);
                        } 
                    })
                },                
                capitalize: function (value) {  // 公司公告 过滤方法
                    var date = new Date(value)
                    Date.prototype.Format = function(fmt){ 
                        var o = {   
                            "M+" : this.getMonth()+1,                 //月份   
                            "d+" : this.getDate(),                    //日
                        };   
                        if(/(y+)/.test(fmt))   
                            fmt=fmt.replace(RegExp.$1, (this.getFullYear()+"").substr(4 - RegExp.$1.length));   
                        for(var k in o)   
                            if(new RegExp("("+ k +")").test(fmt))   
                        fmt = fmt.replace(RegExp.$1, (RegExp.$1.length==1) ? (o[k]) : (("00"+ o[k]).substr((""+ o[k]).length)));   
                        return fmt;   
                    };
                    return date.Format("yyyy-MM-dd");
                },
                getHead: function(){
                    var that = this;   
                    that.$http({      //调用接口
                        method:'GET',
                        url:(this.urlHead + '?code=' + this.code) //this指data
                    }).then(function(response){ //接口返回数据
                        if(response.data.code == 200){
                            this.head = response.data.data;
                        }else{
                            this.lists = response.data.msg;  
                        } 
                    })
                },
                gsggMore: function(){   // 大宗交易更多
                    var that = this;
                    $(".data_more_less").hide(),$(".live-upload").show();
                    if(that.totalPage > that.page){
                        setTimeout(function(){
                            that.page ++;
                            that.getGsgg()
                            $(".live-upload").hide(),$(".data_more_less").show();
                        },500)
                    }else{
                        setTimeout(function(){
                            $(".live-upload").hide(),$(".data_more_less").show();
                            $(".data_more_wrap_gd").removeClass("curr").text("暂无更多数据")
                        },200)
                    }
                },
                lickLess: function(){   // 收起数据
                    var that = this;
                    that.page = 1;
                    that.getGsgg();
                    setTimeout(function(){
                        that.gsggAll = that.gsgg.announcements;
                        $(".data_more_wrap_gd").addClass("curr").text("显示更多")
                    },200)
                }
            }
        })
    };

    // 股本结构 数据请求
    var data_equstr = function (){
        new Vue({
            el:'#container',
            data: {
                lists:[],  
                url: baseUrl + '/capitalStockStructure',  // 存数据接口  
                urlHead: baseUrl + '/profile',  // 存数据接口   
                head: [],   
                message: '暂无数据',
                code: '' ,
                xindata:[], // 股本结构
                lngdbg:[],  // 历年股东变动
                xsjj: [],   // 限售解禁
                xsjjLength: 0     // 限售解禁数量
            },
            created: function(){
                var params = getRequest();
                this.code = params.code;
                this.getHead()
                this.getData()      // 定义方法
            },
            methods: {
                getData: function(){
                    var that = this;   
                    that.$http({      // 调用接口
                        method:'GET',
                        url:(this.url + '?code=' + this.code) // this指data
                    }).then(function(response){ // 接口返回数据
                        if(response.data.code == 200){
                            this.lists = response.data.data;
                            this.xsjj = this.lists.rptRestrictedBanList;
                            this.xsjjLength = this.lists.rptRestrictedBanList.length;
                            this.xindata = response.data.data.capitalStockStructureDetail;
                            this.lngdbg = this.lists.shareChange[0];
                            console.log(this.lists);
                            console.log(this.lngdbg);
                        }else{
                            this.lists = response.data.msg;  
                            console.log(response.data.msg);
                            data_error()
                        } 
                    },function(error){
                        console.log('无效的参数');
                        data_error()
                    })
                },
                getHead: function(){
                    var that = this;   
                    that.$http({      //调用接口
                        method:'GET',
                        url:(this.urlHead + '?code=' + this.code) //this指data
                    }).then(function(response){ //接口返回数据
                        if(response.data.code == 200){
                            this.head = response.data.data;
                        }else{
                            this.lists = response.data.msg;  
                        } 
                    })
                }
            }
        })
    };

    // 公司高管 数据请求
    var data_comexe = function (){
        new Vue({
            el:'#container',
            data: {
                lists:[],  
                url: baseUrl + '/companyManagement',  //存数据接口   
                urlHead: baseUrl + '/profile',  //存数据接口   
                head: [],  
                message: '暂无数据',
                code: '' ,
                ggcgbdLength: 0,  // 高管持股变动数量
                glcjjLength: 0,  // 管理层简介数量
                cszsShu: 5,  // 初始加载数量
                jzgdShu: 5  // 加载更多数量
            },
            created: function(){
                var params = getRequest();
                this.code = params.code;
                this.getHead()
                this.getData()      //定义方法
            },
            methods: {
                getData: function(){
                    var that = this;   
                    that.$http({      //调用接口
                        method:'GET',
                        url:(this.url + '?code=' + this.code) //this指data
                    }).then(function(response){ //接口返回数据
                        if(response.data.code == 200){
                            this.lists = response.data.data;
                            this.ggcgbdLength = this.lists.rptShareHeldChangeList.length;
                            this.glcjjLength = this.lists.rptManagerList.length;
                            console.log(this.lists);
                        }else{
                            this.lists = response.data.msg;  
                            console.log(response.data.msg);
                            data_error()
                        } 
                    },function(error){
                        console.log('无效的参数');
                        data_error()
                    })
                },
                getHead: function(){
                    var that = this;   
                    that.$http({      //调用接口
                        method:'GET',
                        url:(this.urlHead + '?code=' + this.code) //this指data
                    }).then(function(response){ //接口返回数据
                        if(response.data.code == 200){
                            this.head = response.data.data;
                        }else{
                            this.lists = response.data.msg;  
                        } 
                    })
                },
                glcjjMore: function(){   // 管理层简介更多
                    var that = this;
                    $(".data_more_less").hide(),$(".live-upload").show();
                    if(that.cszsShu < that.glcjjLength){
                        setTimeout(function(){
                            $(".live-upload").hide(),$(".data_more_less").show();
                            that.cszsShu = that.cszsShu + that.jzgdShu;
                        },500)
                    }else{
                        setTimeout(function(){
                            $(".live-upload").hide(),$(".data_more_less").show();
                            $(".data_more_wrap_gd").removeClass("curr").text("暂无更多数据")
                        },200)
                    }
                },
                lickLess: function(){   // 收起数据
                    var that = this;
                    that.cszsShu = 5;
                    $(".data_more_wrap_gd").addClass("curr").text("显示更多")
                }
            }
        })
    };

    // 资本运作 数据请求
    var data_cappe = function (){
        new Vue({
            el:'#container',
            data: {
                lists:[],  
                url: baseUrl + '/capitalOperation',  //存数据接口 
                urlHead: baseUrl + '/profile',  //存数据接口   
                head: [],
                message: '暂无数据',
                code: '',
                orderBy: 1,
                isAsc: false,
                xmjdLength: 0,  // 项目进度数量
                cszsShu: 5,  // 初始加载数量
                jzgdShu: 5  // 加载更多数量
            },
            created: function(){
                var params = getRequest();
                this.code = params.code;
                this.getHead()
                this.getData()      //定义方法
            },
            filters: {               //过滤方法
                capitalize: function (value) {
                    var date = new Date(value)
                    Date.prototype.Format = function(fmt){ 
                        var o = {   
                            "M+" : this.getMonth()+1,                 //月份   
                            "d+" : this.getDate(),                    //日
                        };   
                        if(/(y+)/.test(fmt))   
                            fmt=fmt.replace(RegExp.$1, (this.getFullYear()+"").substr(4 - RegExp.$1.length));   
                        for(var k in o)   
                            if(new RegExp("("+ k +")").test(fmt))   
                        fmt = fmt.replace(RegExp.$1, (RegExp.$1.length==1) ? (o[k]) : (("00"+ o[k]).substr((""+ o[k]).length)));   
                        return fmt;   
                    };
                    return date.Format("yyyy-MM-dd");
                }
            },
            methods: {
                getData: function(){
                    var that = this;   
                    that.$http({      //调用接口
                        method:'GET',
                        url:(this.url + '?code=' + that.code + '&orderBy=' + that.orderBy + '&isAsc=' + that.isAsc) //this指data
                    }).then(function(response){ //接口返回数据
                        if(response.data.code == 200){
                            this.lists = response.data.data;
                            this.xmjdLength = this.lists.projectProgress.length;
                            console.log(this.lists);
                        }else{
                            this.lists = response.data.msg;  
                            console.log(response.data.msg);
                            data_error()
                        } 
                    },function(error){
                        console.log('无效的参数');
                        data_error()
                    })
                },
                xmjdMore: function(){   // 项目进度更多数据
                    var that = this;
                    $(".data_more_less").hide(),$(".live-upload").show();
                    if(that.cszsShu < that.xmjdLength){
                        setTimeout(function(){
                            $(".live-upload").hide(),$(".data_more_less").show();
                            that.cszsShu = that.cszsShu + that.jzgdShu;
                        },500)
                    }else{
                        setTimeout(function(){
                            $(".live-upload").hide(),$(".data_more_less").show();
                            $(".data_more_wrap_gd").removeClass("curr").text("暂无更多数据")
                        },200)
                    }
                },
                lickLess: function(){   // 收起数据
                    var that = this;
                    that.cszsShu = 5;
                    $(".data_more_wrap_gd").addClass("curr").text("显示更多")
                },
                getHead: function(){
                    var that = this;   
                    that.$http({      // 调用接口
                        method:'GET',
                        url:(this.urlHead + '?code=' + this.code) // this指data
                    }).then(function(response){ // 接口返回数据
                        if(response.data.code == 200){
                            this.head = response.data.data;
                        }else{
                            this.lists = response.data.msg;  
                        } 
                    })
                }
            }
        })
    };

    // 股东研究 数据请求
    var data_shares = function (){
        new Vue({
            el:'#container',
            data: {
                lists:[],  
                listsChild1:[],
                listsChild2:[],
                listsChild3:[],
                listsChild4:[],
                url: baseUrl + '/shareholderResearch',  // 存数据接口 
                urlTwo: baseUrl + '/mainPositionsHodler',
                urlHead: baseUrl + '/profile',  // 存数据接口   
                head: [],
                focus:{     //焦点
                    curr1: 0,
                    curr2: 0,
                    curr3: 0,
                    curr4: 0
                },                                           
                message: '暂无数据',
                code: '',
                date: '',
                sdgdcgbdLength: 0,     // 十大股东持股变动数量
                xsjjLength: 0     // 限售解禁数量
            },
            created: function(){
                var params = getRequest();
                this.code = params.code;
                this.data = params.data;
                this.getHead()
                this.getData()      //定义方法
            },
            filters: {               //过滤方法
                toThousands: function(num){
                    if(!isNaN(num)){
                        var num = (num || 0).toString(), result = '';
                        while (num.length > 3) {
                            result = ',' + num.slice(-3) + result;
                            num = num.slice(0, num.length - 3);
                        }
                        if (num) { result = num + result; }
                        return result;
                    }else{
                        return null;
                    }
                }
            },
            methods: {
                getData: function(){
                    var that = this;   
                    that.$http({      //调用接口
                        method:'GET',
                        url:(this.url + '?code=' + this.code) //this指data
                    }).then(function(response){ //接口返回数据
                        if(response.data.code == 200){
                            this.lists = response.data.data;
                            this.listsChild1 = this.lists.sdltgd[0].sdltgd;
                            this.listsChild2 = this.lists.sdgd[0].sdgd;
                            this.listsChild3 = this.lists.zlcc;
                            this.listsChild4 = this.lists.jjcg[0].jjcg;
                            this.sdgdcgbdLength = this.lists.sdgdcgbd.length;
                            this.xsjjLength = this.lists.xsjj.length;
                            console.log(this.lists);
                        }else{
                            this.lists = response.data.msg;  
                            console.log(response.data.msg);
                            data_error()
                        } 
                    },function(error){
                        console.log('无效的参数');
                        data_error()
                    })
                },
                getDataTwo: function(){
                    var that = this;   
                    that.$http({      //调用接口
                        method:'GET',
                        url:(this.urlTwo + '?code=' + this.code + '&date=' + this.date) //this指data
                    }).then(function(response){ //接口返回数据
                        if(response.data.code == 200){
                            this.listsChild3 = response.data.data;
                        }else{
                            this.lists = response.data.msg;  
                            console.log(response.data.msg);
                            data_error()
                        } 
                    },function(error){
                        console.log('无效的参数');
                        data_error()
                    })
                },
                toggleOne(i, list){   // 选项卡 
                    this.focus.curr1 = i;
                    this.listsChild1 = list;
                },
                toggleTwo(i, list){   // 选项卡 
                    this.focus.curr2 = i;
                    this.listsChild2 = list;
                },
                toggleThree(i, list){   // 选项卡 
                    this.focus.curr3 = i;
                    this.date = list;
                    this.getDataTwo();
                },
                toggleFour(i, list){   // 选项卡 
                    this.focus.curr4 = i;
                    this.listsChild4 = list;
                },
                getHead: function(){
                    var that = this;   
                    that.$http({      //调用接口
                        method:'GET',
                        url:(this.urlHead + '?code=' + this.code) //this指data
                    }).then(function(response){ //接口返回数据
                        if(response.data.code == 200){
                            this.head = response.data.data;
                        }else{
                            this.lists = response.data.msg;  
                        } 
                    })
                }
            }
        })
    };

    // 公司概况 数据请求
    var data_survey = function (){
        new Vue({
            el:'#container',
            data: {
                lists:[],  
                url: baseUrl + '/companySurvey',  //存数据接口  
                urlHead: baseUrl + '/profile',  //存数据接口   
                head: [],   
                message: '暂无数据',
                code: '',
                jbzl:[],
                fxxg:[]
            },
            created: function(){
                var params = getRequest();
                this.code = params.code;
                this.getHead()
                this.getData()      // 定义方法
            },
            methods: {
                getData: function(){
                    var that = this;   
                    that.$http({      //调用接口
                        method:'GET',
                        url:(this.url + '?code=' + this.code) //this指data
                    }).then(function(response){ //接口返回数据
                        if(response.data.code == 200){
                            that.lists = response.data.data;
                            that.jbzl = that.lists.jbzl;
                            that.fxxg = that.lists.fxxg;
                            console.log(that.jbzl,that.fxxg);
                        }else{
                            that.lists = response.data.msg;  
                            console.log(response.data.msg)
                            data_error();
                        } 
                    },function(error){
                        data_error();
                        console.log('无效的参数')
                    })
                },
                getHead: function(){
                    var that = this;   
                    that.$http({      //调用接口
                        method:'GET',
                        url:(this.urlHead + '?code=' + this.code) //this指data
                    }).then(function(response){ //接口返回数据
                        if(response.data.code == 200){
                            this.head = response.data.data;
                        }else{
                            this.lists = response.data.msg;  
                        } 
                    })
                }
            }
        })
    };

    // 财务分析 数据请求
    var data_analy = function (){
        new Vue({
            el:'#container',
            data: {
                lists:[],                       // 主要指标
                zcfzb:[],                       // 资产负债表
                lrb:[],                         // 利润表
                xjllb:[],                       // 现金流量表
                bfbbb:[],                       // 百分比报表
                bfbbbOne:[],                    //  百分比报表 第一条数据
                url: baseUrl + '/mainTarget',   // 主要指标 存数据接口     
                url_zcfzb: baseUrl + '/zcfzb',  // 资产负债表 存数据接口   
                url_lrb: baseUrl + '/lrb',      // 利润表 存数据接口   
                url_xjllb: baseUrl + '/xjllb',  // 现金流量表 存数据接口   
                url_bfbbb: baseUrl + '/percent',// 百分比报表 存数据接口   
                urlHead: baseUrl + '/profile',  // 存数据接口   
                head: [],
                message: '暂无数据',
                code: '',
                type: 0,                         // 主要指标 type
                type_bfbbb: 0,                   // 百分比报表 type
                reportDateType: 0,               // 报告日期类型
                reportType: 1,                   // 报告类型
                zcfzb_sy: 0,                     // 资产负债表 利润表 现金流量表 索引
                size: 12,                         // 资产负债表 利润表 现金流量表 每一页长度
                page: 1,                         // 资产负债表 利润表 现金流量表 当前页
                zcfzb_Index: 1,                  // 资产负债表 切换列表索引
                lrb_Index: 1,                    // 利润 切换列表索引
                xjllb_Index: 1,                  // 现金流量 切换列表索引
                zcfzbLength:6,
                lrbLength:6,
                xjllbLength:6
            },
            created: function(){
                var params = getRequest();
                this.code = params.code;
                this.getHead()
                this.getData();             // 主要指标 初始化
                this.getDataZcfzb();        // 资产负债表 初始化
                this.getDataLrb();          // 利润表 初始化
                this.getDataXjllb();        // 现金流量表 初始化
                this.getDataBfbbb();        // 百分比报表 初始化
            },
            filters: {               // 过滤方法
                formatNumber: function(value) {             // 模板数值格式化
                    var temp = Number(value);
                    if (!temp)
                        return "--";
                    if (Math.abs(temp) >= 1e12) {
                        var resupt = temp / 1e12;
                        return resupt.toFixed(2) + "万亿";
                    }
                    else if (Math.abs(temp) >= 1e8) {
                        var resupt = temp / 1e8;
                        return resupt.toFixed(2) + "亿";
                    }
                    else if (Math.abs(temp) >= 1e4) {
                        var resupt = temp / 1e4;
                        return resupt.toFixed(2) + "万";
                    }
                    return temp;
                },
                formatDate: function(value) {
                    if (value.indexOf("/") > 0) {
                        var ss = "";
                        ss.substr
                        var temp = value.split("/");
                        return temp[0] + "-" + (temp[1] > 9 ? temp[1] : "0" + temp[1]) + "-" + temp[2].split(" ")[0];
                    }
                    else if (value.indexOf("-") > 0) {
                        var ss = "";
                        ss.substr
                        var temp = value.split("-");
                        return temp[0] + "-" + (temp[1] > 9 ? temp[1] : "0" + temp[1]) + "-" + temp[2].split(" ")[0];
                    }
                    else
                        return "--";
                },
                formatRate: function(value) {
                    var temp = Number(value);
                    if (!temp)
                        return "--";
                    return temp.toFixed(2) + "%";
                }
            },
            methods: {
                getData: function(){      // 主要指标 调用接口
                    var that = this;   
                    that.$http({
                        method:'GET',
                        url:(this.url + '?code=' + this.code +'&type=' + this.type) // this指data
                    }).then(function(response){ // 接口返回数据
                        if(response.data.code == 200){
                            that.lists = response.data.data;
                            that.clickTableTrBtn();
                        }else{ 
                            console.log(response.data.msg);
                            data_error()
                        } 
                    },function(error){
                        console.log('无效的参数');
                        data_error()
                    })
                },
                clickTableTrBtn: function(){ // 主要指标  排序方法
                    var that = this;
                    $('.data_analy_zyzb_head li').unbind().click(function() {
                        that.type = $(this).index();
                        $(this).addClass('curr').siblings().removeClass('curr');
                        that.getData();
                        $(".data_analy_zyzb_body").css({'transition-duration':'1000ms', 'transform': 'translate3d(0px, 0px, 0px)'})
                    })
                },
                zcfzb_hide: function(){ // 资产负债表  隐藏空数据
                    $(".data_analy_zcfzb_body > div").each(function(){
                        let arr = [];
                        let arrNew = [];
                        $(this).find("span").each(function(){
                            arr.push($(this).text());
                        })
                        arr.forEach((str)=>{
                            if(str != "--"){
                                arrNew.push(str);
                            }
                        })
                        if(arrNew.length == 0){
                            $(this).hide();
                            var _index = $(this).index()
                            $(".data_analy_zcfzb_le > div").eq(_index).hide()
                        }else{
                            $(this).show();
                            var _inde = $(this).index()
                            $(".data_analy_zcfzb_le > div").eq(_inde).show()
                        }
                    })
                },
                getDataZcfzb: function(){      // 资产负债表 调用接口
                    var that = this;   
                    that.$http({
                        method:'GET',
                        url:(this.url_zcfzb + '?reportDateType=' + this.reportDateType + '&reportType=' + this.reportType + '&code=' + this.code + '&page=' + this.page + '&size=' + this.size) // this指data
                    }).then(function(response){ // 接口返回数据
                        if(response.data.code == 200){
                            that.zcfzb = response.data.data;
                            console.log(that.zcfzb)
                            that.clickTableZcfzb();
                            setTimeout(function(){that.zcfzb_hide();},100)
                        }else{
                            console.log(response.data.msg);
                        } 
                    },function(error){
                        console.log('无效的参数');
                    })
                },
                zcfzbClick: function(a,b){ // 资产负债表  排序方法
                    var that = this;
                    that.zcfzb = [];
                    that.reportDateType = a;
                    that.reportType = b;
                    that.getDataZcfzb();
                    $(".data_analy_zyzb_body").css({'transition-duration':'1000ms', 'transform': 'translate3d(0px, 0px, 0px)'})
                },
                clickTableZcfzb: function(){ 
                    var that = this;
                    $('.data_analy_zcfzb_head li').unbind().click(function() {  // 资产负债表 利润表 现金流量表  焦点样式
                        that.zcfzb_sy = $(this).index();
                        $(this).addClass('curr').siblings().removeClass('curr');
                    });
                },
                lrb_hide: function(){ // 利润表  隐藏空数据
                    $(".data_analy_lrb_body > div").each(function(){
                        let arr = [];
                        let arrNew = [];
                        $(this).find("span").each(function(){
                            arr.push($(this).text());
                        })
                        arr.forEach((str)=>{
                            if(str != "--"){
                                arrNew.push(str);
                            }
                        })
                        if(arrNew.length == 0){
                            $(this).hide(); 
                            var _index = $(this).index()
                            $(".data_analy_lrb_le > div").eq(_index).hide()
                        }else{
                            $(this).show();
                            var _index = $(this).index()
                            $(".data_analy_lrb_le > div").eq(_index).show()
                        }
                    })
                },
                getDataLrb: function(){      // 利润表 调用接口
                    var that = this;   
                    that.$http({
                        method:'GET',
                        url:(this.url_lrb + '?reportDateType=' + this.reportDateType + '&reportType=' + this.reportType + '&code=' + this.code + '&page=' + this.page + '&size=' + this.size) // this指data
                    }).then(function(response){ // 接口返回数据
                        if(response.data.code == 200){
                            that.lrb = response.data.data;
                            that.clickTableZcfzb();
                            setTimeout(function(){that.lrb_hide();},100)
                        }else{
                            console.log(response.data.msg);
                        } 
                    },function(error){
                        console.log('无效的参数');
                    })
                },
                lrbClick: function(a,b){ // 利润表  排序方法
                    var that = this;
                    that.reportDateType = a;
                    that.reportType = b;
                    that.getDataLrb();
                    $(".data_analy_zyzb_body").css({'transition-duration':'1000ms', 'transform': 'translate3d(0px, 0px, 0px)'})
                },
                xjllb_hide: function(){ // 现金流量表  隐藏空数据
                    $(".data_analy_xjllb_body > div").each(function(){
                        let arr = [];
                        let arrNew = [];
                        $(this).find("span").each(function(){
                            arr.push($(this).text());
                        })
                        arr.forEach((str)=>{
                            if(str != "--"){
                                arrNew.push(str);
                            }
                        })
                        if(arrNew.length == 0){
                            $(this).hide(); 
                            var _index = $(this).index()
                            $(".data_analy_xjllb_le > div").eq(_index).hide()
                        }else{
                            $(this).show();
                            var _index = $(this).index()
                            $(".data_analy_xjllb_le > div").eq(_index).show()
                        }
                    })
                },
                getDataXjllb: function(){
                    var that = this;   
                    that.$http({      // 现金流量表 调用接口
                        method:'GET',
                        url:(this.url_xjllb + '?reportDateType=' + this.reportDateType + '&reportType=' + this.reportType + '&code=' + this.code + '&page=' + this.page + '&size=' + this.size) // this指data
                    }).then(function(response){ // 接口返回数据
                        if(response.data.code == 200){
                            that.xjllb = response.data.data;
                            that.clickTableZcfzb();
                            setTimeout(function(){that.xjllb_hide();},100)
                        }else{
                            console.log(response.data.msg);
                        } 
                    },function(error){
                        console.log('无效的参数');
                    })
                },
                xjllbClick: function(a,b){ // 现金流量表  排序方法
                    var that = this;
                    that.reportDateType = a;
                    that.reportType = b;
                    that.getDataXjllb();
                    $(".data_analy_zyzb_body").css({'transition-duration':'1000ms', 'transform': 'translate3d(0px, 0px, 0px)'})
                },
                getDataBfbbb: function(){
                    var that = this;   
                    that.$http({      //百分比报表 调用接口
                        method:'GET',
                        url:(this.url_bfbbb + '?code=' + this.code +'&type=' + this.type_bfbbb) //this指data
                    }).then(function(response){ //接口返回数据
                        if(response.data.code == 200){
                            that.bfbbb = response.data.data.lr0;
                            that.bfbbbOne = that.bfbbb[0];
                            that.clickTableBfbbb();
                        }else{ 
                            console.log(response.data.msg);
                            data_error()
                        } 
                    },function(error){
                        console.log('无效的参数');
                        data_error()
                    })
                },
                clickTableBfbbb: function(){ //百分比报表  排序方法
                    var that = this;
                    $('.data_analy_bfbbb_head li').unbind().click(function() {
                        that.bfbbb = [];
                        that.type_bfbbb = $(this).index();
                        $(this).addClass('curr').siblings().removeClass('curr');
                        that.getDataBfbbb();
                        $(".data_analy_zyzb_body").css({'transition-duration':'1000ms', 'transform': 'translate3d(0px, 0px, 0px)'})
                    })
                },
                getHead: function(){
                    var that = this;   
                    that.$http({      //调用接口
                        method:'GET',
                        url:(this.urlHead + '?code=' + this.code) //this指data
                    }).then(function(response){ //接口返回数据
                        if(response.data.code == 200){
                            this.head = response.data.data;
                        }else{
                            this.lists = response.data.msg;  
                        } 
                    })
                }
            }
        })
    };

    // 最新动态 数据请求
    var data_news = function (){
        new Vue({
            el:'#container',
            data: {
                lists:[],  
                gsgg:[],  
                zyzb: [],                                   // 主要指标
                zybl:[],                                   // 质押比例
                zygdtj:[],                                 // 重要股东统计
                url: baseUrl + '/profile',                 // 存数据接口     
                urlGsgg: baseUrl + '/getAnnouncements',    // 公司公告 存数据接口  
                url_gsds: baseUrl + '/companyBigNews',     // 公司大事 存数据接口 
                dzjyLength: 0,                             // 公司大事 大宗交易数量  
                url_zyzb: baseUrl + '/mainTarget',         // 主要指标 存数据接口    
                url_zybl: baseUrl + '/queryPledgebl',      // 质押比例 存数据接口   
                url_zygdtj: baseUrl + '/queryPartner',     // 重要股东统计 存数据接口  
                reportDateType: 0,                         // 报告日期类型
                reportType: 1,                             // 报告类型
                gsds: [],
                message: '暂无数据',
                inputValue: '',
                currPage: 1,                               // 公司公告 资产负债表 利润表 当前也
                pageSize: 10,                              // 公司公告 每页条数
                beginDate: '2000-01-01',                   // 公司公告 开始时间 最早
                endDate: '',                               // 公司公告 结束时间
                type: 0,                                   // 主要指标 type
                code: '',
                gsggLength: 0
            },
            created: function(){
                var params = getRequest();
                this.code = params.code;
                this.getData()      //  定义方法
                this.getDate()
                this.getGsds()
                this.getGsgg()
                this.getDataZyzb()
                this.getDataZybl()
                this.getDataZygdtj()
            },
            filters: {               // 过滤方法
                formatNumberZygs: function(value) {             //  模板数值格式化
                    var temp = Number(value);
                    if (!temp)
                        return 0;
                    if (Math.abs(temp) >= 1e8) {
                        var resupt = temp / 1e8;
                        return resupt.toFixed(2) + "万亿";
                    }else if (Math.abs(temp) >= 1e4) {
                        var resupt = temp / 1e4;
                        return resupt.toFixed(2) + "亿";
                    }else if (Math.abs(temp) >= 1) {
                        return temp.toFixed(2) + "万";
                    }
                    return (temp*10000);
                },
                formatNumberXsd: function(value) {             //  模板两位小数点格式化
                    if (!value || value === "-" ||  value === "--"){
                        return '-';
                    }
                    var temp = Number(value);
                    return temp.toFixed(2);
                }
            },
            methods: {
                getDate: function(){
                    var that = this;
                    function getNow(s) {
                        return s < 10 ? '0' + s: s;
                    }
                    var myDate = new Date();
                    var year=myDate.getFullYear();  //    获取当前年
                    var month=myDate.getMonth()+1;  //    获取当前月
                    var date=myDate.getDate();  //    获取当前日
                    that.endDate = year+'-'+getNow(month)+"-"+getNow(date)
                },
                getData: function(){
                    var that = this;   
                    that.$http({      //    调用接口
                        method:'GET',
                        url:(this.url + '?code=' + this.code) //    this指data
                    }).then(function(response){ //  接口返回数据
                        if(response.data.code == 200){
                            this.lists = response.data.data;
                            console.log(this.lists);
                        }else{
                            this.lists = response.data.msg;  
                            console.log(response.data.msg)
                            data_error();
                        } 
                    },function(error){
                        data_error();
                        console.log('无效的参数')
                    })
                },
                cliackBut: function(){
                    var _href = $(".data_button").attr("data");
                    console.log($(".data_input").val().length)
                    if($(".data_input").val().length <= 2){
                        alert('请输入正确的股票代码');
                        $(".data_button").attr("disabled",true).removeAttr("target").removeAttr("href");
                    }else{
                        $(".data_button").removeAttr("disabled").attr("target","_blank").attr("href",_href);
                    }
                },
                getGsds: function(){
                    var that = this;   
                    that.$http({      //    调用接口
                        method:'GET',
                        url:(this.url_gsds + '?code=' + this.code) //   this指data
                    }).then(function(response){ //  接口返回数据
                        if(response.data.code == 200){
                            this.gsds = response.data.data;
                            this.dzjyLength = this.gsds.dzjy.length;
                        }else{
                            this.gsds = response.data.msg;  
                            data_error()
                        } 
                    })
                },
                getGsgg: function(){
                    var that = this;   
                    that.$http({      //    调用接口
                        method:'GET',
                        url:(this.urlGsgg + '?code=' + this.code + '&currPage=' + this.currPage + '&pageSize=' + this.pageSize + '&beginDate=' + this.beginDate + '&endDate=' + this.endDate) //    this指data
                    }).then(function(response){ //  接口返回数据
                        if(response.data.code == 200){
                            this.gsgg = response.data.data;
                            this.gsggLength = this.gsgg.announcements.length;
                            console.log(this.gsgg)
                        } 
                    })
                },               
                capitalize: function (value) {  //  过滤方法
                    var date = new Date(value)
                    Date.prototype.Format = function(fmt){ 
                        var o = {   
                            "M+" : this.getMonth()+1,                 //    月份   
                            "d+" : this.getDate(),                    //    日
                        };   
                        if(/(y+)/.test(fmt))   
                            fmt=fmt.replace(RegExp.$1, (this.getFullYear()+"").substr(4 - RegExp.$1.length));   
                        for(var k in o)   
                            if(new RegExp("("+ k +")").test(fmt))   
                        fmt = fmt.replace(RegExp.$1, (RegExp.$1.length==1) ? (o[k]) : (("00"+ o[k]).substr((""+ o[k]).length)));   
                        return fmt;   
                    };
                    return date.Format("yyyy-MM-dd");
                },
                getDataZyzb: function(){      //    主要指标 调用接口
                    var that = this;   
                    that.$http({
                        method:'GET',
                        url:(this.url_zyzb + '?code=' + this.code +'&type=' + this.type) //  this指data
                    }).then(function(response){ //  接口返回数据
                        if(response.data.code == 200){
                            that.zyzb = response.data.data[0];
                            console.log(that.zyzb)
                        }
                    })
                },
                getDataZybl: function(){     // 质押比例 调用接口
                    var that = this;   
                    that.$http({      
                        method:'GET',
                        url:(this.url_zybl + '?code=' + this.code)
                    }).then(function(response){
                        if(response.data.code == 200){
                            that.zybl = response.data.data.content;
                        } 
                    })
                },
                getDataZygdtj: function(){     // 重要股东统计 调用接口
                    var that = this;   
                    that.$http({      
                        method:'GET',
                        url:(this.url_zygdtj + '?code=' + this.code)
                    }).then(function(response){
                        if(response.data.code == 200){
                            that.zygdtj = response.data.data.content;
                        } 
                    })
                }
            }
        })
    };

    // 固定信息栏
    $(window).scroll(function(){
        if($(".datanews_con_nav_wrap").length > 0){
            var _windowScr = $(window).scrollTop();
            var data_nav = $(".datanews_con_nav_wrap").offset().top;
            if(_windowScr >= data_nav){
                $(".datanews_con_nav_wrap").addClass('curr');
            }else{
                $(".datanews_con_nav_wrap").removeClass('curr');
            };
        }
        if($(".datanews_con_nav2_wrap").length > 0){
            var _windowScr = $(window).scrollTop();
            var data_nav = $(".datanews_con_nav2_wrap").offset().top;
            if(_windowScr >= data_nav){
                $(".datanews_con_nav2_wrap").addClass('curr');
            }else{
                $(".datanews_con_nav2_wrap").removeClass('curr');
            };
        }
    })

    // 展开 收起
    var data_zksq = function (){
        $(".data_survey_zksq_wrap").each(function(){
            var _this = $(this)
            var str;
            var sTitle =_this.children(".data_survey_zksq").text();
            if(sTitle.length > 60){
                str = sTitle.substring(0,50)+"...";
                _this.children(".toggle-wrap").css({"display":"inline-block"})
            }
            _this.children(".data_survey_zksq").text(str);
            _this.children(".toggle-wrap").click(function(){
                _this.find(".icon-square").toggleClass("expand");
                if(_this.find(".toggle-label").text() === "展开"){
                    _this.find(".data_survey_zksq").text(sTitle);
                    _this.find(".toggle-label").text("收起");
                }else{
                    _this.find(".data_survey_zksq").text(str);
                    _this.find(".toggle-label").text("展开");
                }
                
            })
        })
    }

    // 行业一览 数据请求
    var data_hyyl = function (){
        new Vue({
            el:'#container',
            data: {
                lists:[],
                url: baseUrl + '/indexThshy',  //存数据接口 
                message: '暂无数据',
                code: '',
                field: '199112',
                order: 'desc',
                page: '1',
                pageSize: 50, // 每一页显示的数据条数
                maxPage: 9 , // 最大页数
                lengthCurr: Number // 当前页数的条数
            },
            created: function(){
                this.getData();    //定义方法
            },
            methods: {
                getData: function(){
                    var that = this;   
                    that.$http({      //调用接口
                        method:'GET',
                        url:(this.url + '?field=' + this.field + '&order=' + this.order + '&page=' + this.page) //this指data
                    }).then(function(response){ //接口返回数据
                        if(response.data.code == 200){
                            this.lists = response.data.data;
                            this.lengthCurr = this.lists.length;
                        }else{
                            this.lists = response.data.msg;  
                            console.log(response.data.msg);
                            data_error()
                        } 
                    },function(error){
                        data_error()
                        console.log('无效的参数');
                    })
                },
                sortChange: function(column, prop, order){
                    var that = this;
                    console.log(column + '-' + column.prop + '-' + column.order)
                    switch (column.prop) {
                        case "hyzdf_bfb":
                            that.field = "199112";
                            that.order = (column.order === "ascending")?'asc':'desc';
                            that.getData();
                            break;
                        case "zcjl":
                            that.field = "13";
                            that.order = (column.order === "ascending")?'asc':'desc';
                            that.getData();
                            break;
                        case "zcje":
                            that.field = "19";
                            that.order = (column.order === "ascending")?'asc':'desc';
                            that.getData();
                            break;
                        case "jlr":
                            that.field = "zjjlr";
                            that.order = (column.order === "ascending")?'asc':'desc';
                            that.getData();
                            break;
                        case "jj":
                            that.field = "330184";
                            that.order = (column.order === "ascending")?'asc':'desc';
                            that.getData();
                            break;
                        default:
                            break;
                    }
                },
                dataMore: function(){   //  加载更多数据
                    var that = this;
                    $(".data_more_less").hide(),$(".live-upload").show();
                    if(that.lengthCurr < that.pageSize){
                        setTimeout(function(){
                            $(".live-upload").hide(),$(".data_more_less").show();
                            $(".data_more_wrap_gd").removeClass("curr").text("暂无更多数据")
                        },200)
                    }else{
                        setTimeout(function(){
                            that.page ++;
                            that.getData()
                            $(".live-upload").hide(),$(".data_more_less").show();
                        },500)
                    }
                },
                lickLess: function(){      // 收起数据
                    var that = this;
                    that.page = 1;
                    that.getData();
                    $(".data_more_wrap_gd").addClass("curr").text("显示更多")
                }
            }
        })
    };

    // 行业一览详情 数据请求
    var data_hyyl_xq = function (){
        new Vue({
            el:'#container',
            data: {
                lists:[],  
                url: baseUrl + '/detailThshy',  //存数据接口   
                message: '暂无数据',
                code: '',
                field: '199112',
                order: 'desc',
                page: '1',
                pageSize: 20, // 每一页显示的数据条数
                maxPage:9,  // 最大页数
                lengthCurr: Number // 当前页数的条数
            },
            created: function(){
                var params = getRequest();
                this.code = params.code;
				this.getData();    //定义方法
            },
            methods: {
                getData: function(){
                    var that = this;   
                    that.$http({      //调用接口
                        method:'GET',
                        url:(this.url + '?field=' + this.field + '&order=' + this.order + '&page=' + this.page + '&code=' + this.code) //this指data
                    }).then(function(response){ //接口返回数据
                        if(response.data.code == 200){
                            this.lists = response.data.data;
                            console.log(this.lists)
                            this.lengthCurr = response.data.data.length;
                        }else{
                            this.lists = response.data.msg;  
                            console.log(response.data.msg);
                            data_error()
                        } 
                    },function(error){
                        data_error()
                        console.log('无效的参数');
                    })
                },
                sortChange: function(column, prop, order){
                    var that = this;
                    console.log(column + '-' + column.prop + '-' + column.order)
                    switch (column.prop) {
                        case "xj":
                            that.field = "10";
                            that.order = (column.order === "ascending")?'asc':'desc';
                            that.getData();
                            break;
                        case "zdf":
                            that.field = "199112";
                            that.order = (column.order === "ascending")?'asc':'desc';
                            that.getData();
                            break;
                        case "zd":
                            that.field = "264648";
                            that.order = (column.order === "ascending")?'asc':'desc';
                            that.getData();
                            break;
                        case "zs":
                            that.field = "48";
                            that.order = (column.order === "ascending")?'asc':'desc';
                            that.getData();
                            break;
                        case "hs":
                            that.field = "1968584";
                            that.order = (column.order === "ascending")?'asc':'desc';
                            that.getData();
                            break;
                        case "lb":
                            that.field = "1771976";
                            that.order = (column.order === "ascending")?'asc':'desc';
                            that.getData();
                            break;
                        case "zf":
                            that.field = "526792";
                            that.order = (column.order === "ascending")?'asc':'desc';
                            that.getData();
                            break;
                        case "cje":
                            that.field = "19";
                            that.order = (column.order === "ascending")?'asc':'desc';
                            that.getData();
                            break;
                        case "ltg":
                            that.field = "407";
                            that.order = (column.order === "ascending")?'asc':'desc';
                            that.getData();
                            break;
                        case "ltsz":
                            that.field = "3475914";
                            that.order = (column.order === "ascending")?'asc':'desc';
                            that.getData();
                            break;
                        case "syl":
                            that.field = "2034120";
                            that.order = (column.order === "ascending")?'asc':'desc';
                            that.getData();
                            break;
                        default:
                            break;
                    }
                },
                dataMore: function(){   //  加载更多数据
                    var that = this;
                    $(".data_more_less").hide(),$(".live-upload").show();
                    if(that.lengthCurr < that.pageSize){
                        setTimeout(function(){
                            $(".live-upload").hide(),$(".data_more_less").show();
                            $(".data_more_wrap_gd").removeClass("curr").text("暂无更多数据")
                        },200)
                    }else{
                        setTimeout(function(){
                            that.page ++;
                            that.getData()
                            $(".live-upload").hide(),$(".data_more_less").show();
                        },500)
                    }
                },
                lickLess: function(){      // 收起数据
                    var that = this;
                    that.page = 1;
                    that.getData();
                    $(".data_more_wrap_gd").addClass("curr").text("显示更多")
                }
            }
        })
    };

    // 股东增减持 数据请求
    var data_gdzjc = function (){
        new Vue({
            el:'#container',
            data: {
                lists:[], 
                list:[],  
                listAll:[],  
                url: baseUrl + '/detailindecrease',  //存数据接口   
                message: '暂无数据',
                code: '',
                page: '1',    // 当前页数
                totalPage: Number,  // 总共页数
                pageSize: 50, // 每一页显示的数据条数
                total: Number, // 记录总数
                maxPage:9  // 最大页数
            },
            created: function(){
				this.getData();    //定义方法
            },
            methods: {
                getData: function(){
                    var that = this;   
                    that.$http({      //调用接口
                        method:'GET',
                        url:(this.url + '?currPage=' + this.page + '&pageSize=' + this.pageSize ) //this指data
                    }).then(function(response){ //接口返回数据
                        if(response.data.code == 200){
                            this.lists = response.data.data;
                            this.list = this.lists.content;
                            this.listAll = this.listAll.concat(this.list);
                            this.total = this.lists.totalElements;
                            this.totalPage = Math.ceil(this.total / this.pageSize);
                            console.log(this.lists)
                        }else{
                            this.lists = response.data.msg;  
                            console.log(response.data.msg);
                            data_error()
                        } 
                    },function(error){
                        data_error()
                        console.log('无效的参数');
                    })
                },
                formatRate: function(value) { // 过滤方法
                    var temp = Number(value);
                    if (!temp){
                        return "--";
                    }
                    return temp.toFixed(2);
                },
                formatRatebfb: function(value) { // 过滤方法
                    var temp = Number(value);
                    if (!temp){
                        return "--";
                    }
                    return temp.toFixed(2) + "%";
                },
                dataMore: function(){   //  加载更多数据
                    var that = this;
                    $(".data_more_less").hide(),$(".live-upload").show();
                    if(that.totalPage > that.page){
                        setTimeout(function(){
                            that.page ++;
                            that.getData()
                            $(".live-upload").hide(),$(".data_more_less").show();
                        },500)
                    }else{
                        setTimeout(function(){
                            $(".live-upload").hide(),$(".data_more_less").show();
                            $(".data_more_wrap_gd").removeClass("curr").text("暂无更多数据")
                        },200)
                    }
                },
                lickLess: function(){      // 收起数据
                    var that = this;
                    that.page = 1;
                    that.getData();
                    setTimeout(function(){
                        that.listAll = that.list;
                        $(".data_more_wrap_gd").addClass("curr").text("显示更多")
                    },200)
                }
            }
        })
    };
    
    // 沪深港股通 数据请求
    var data_hsggtb = function (){
        new Vue({
            el:'#container',
            data: {
                lists:[],  
                list:[], 
                url: baseUrl + '/indexHsgt',  //存数据接口   
                message: '暂无数据',
                code: '',
                page: 1,
                totalPage: Number, // 总页数
                pageSize: 50, // 每一页显示的数据条数
                total: Number, // 记录总数
                maxPage: 9,  // 最大页数
                hsgtType: ''
            },
            created: function(){
                var params = getRequest();
                this.hsgtType = params.code;
				this.getData();    //定义方法
            },
            methods: {
                getData: function(){
                    var that = this;   
                    that.$http({      //调用接口
                        method:'GET',
                        url:(this.url + '?hsgtType=' + this.hsgtType + '&currPage=' + this.page + '&pageSize=' + this.pageSize) //this指data
                    }).then(function(response){ //接口返回数据
                        if(response.data.code == 200){
                            this.list = response.data.data.content;
                            this.lists = this.lists.concat(this.list);
                            this.total = response.data.data.totalElements;
                            this.totalPage = Math.ceil(this.total / this.pageSize);
                            console.log(this.list);
                            console.log(this.list.length);
                            console.log(this.lists.length);
                        }else{
                            this.lists = response.data.msg;  
                            console.log(response.data.msg);
                            data_error()
                        } 
                    },function(error){
                        data_error()
                        console.log('无效的参数');
                    })
                },
                intercept: function(value) { // 删除最后一个字符
                    var val = value.substring(0,value.length-1)*1;
                    return val;
                },
                dataMore: function(){   //  加载更多数据
                    var that = this;
                    $(".data_more_less").hide(),$(".live-upload").show();
                    if(that.totalPage > that.page){
                        setTimeout(function(){
                            that.page ++;
                            that.getData()
                            $(".live-upload").hide(),$(".data_more_less").show();
                        },500)
                    }else{
                        setTimeout(function(){
                            $(".live-upload").hide(),$(".data_more_less").show();
                            $(".data_more_wrap_gd").removeClass("curr").text("暂无更多数据")
                        },200)
                    }
                },
                lickLess: function(){      // 收起数据
                    var that = this;
                    that.page = 1;
                    that.getData();
                    $(".data_more_less").hide(),$(".live-upload").show();
                    setTimeout(function(){
                        that.lists = that.list;
                        $(".live-upload").hide(),$(".data_more_less").show();
                        $(".data_more_wrap_gd").addClass("curr").text("显示更多")
                    },200)
                }
            }
        })
    };

    // 返回上一页
    var data_storeHerf = function(){
        if($(".project_nav").length > 0){
            $(".project_nav > li").on('click', function(){
                var nameHref = $(this).find('a').attr('href');
                localStorage.nameHref = nameHref;
            })
        }
    }
    var data_returnHerf = function(){
        var returnHref = localStorage.nameHref;
        if(returnHref){
            $(".return_href").attr("onclick","returnTest()");
        }
    }

    return {
        data_head_wrap: data_head_wrap,
        data_bulk: data_bulk,
        data_divfin: data_divfin,
        data_resrep: data_resrep,
        data_equstr: data_equstr,
        data_comexe: data_comexe,
        data_cappe: data_cappe,
        data_shares: data_shares,
        data_survey: data_survey,
        data_analy: data_analy,
        data_news: data_news,
        getRequest: getRequest,
        data_zksq: data_zksq,
        data_error: data_error,
        data_hyyl: data_hyyl,
        data_hyyl_xq: data_hyyl_xq,
        data_gdzjc: data_gdzjc,
        data_hsggtb: data_hsggtb,
        data_storeHerf: data_storeHerf,
        data_returnHerf: data_returnHerf
    }
    
});